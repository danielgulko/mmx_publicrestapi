package com.magnet.mmx.restassured;

import junit.framework.TestCase;
import org.apache.http.entity.ContentType;
import org.junit.Before;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.baseURI;
import static org.hamcrest.CoreMatchers.*;
import static com.jayway.restassured.RestAssured.*;


public class MessageStates extends TestCase {
    String baseURL = "http://207.135.69.242:9090/plugins/mmxmgmt/messages";
    String user = "admin";
    String pass = "admin";
    String appId = "cnti72by3d3";


    @Test
    public void testMessageStateDelivered() throws Exception {
        String baseURI = baseURL;
        Object result = null;
        given().log().all().
                queryParam("appId", appId).
                queryParam("size", "50").
                queryParam("searchby", "state").
                queryParam("value", "DELIVERED").
                authentication().preemptive().basic(user, pass).
        when().get(baseURL).
        then().body("results.state", hasItem("DELIVERED")).statusCode(200);
    }

    @Test
    public void testMessageStateAttempted() throws Exception {
        String baseURI = baseURL;
        Object result = null;
        given().log().all().
                queryParam("appId", appId).
                queryParam("size", "50").
                queryParam("searchby", "state").
                queryParam("value", "DELIVERY_ATTEMPTED").
                authentication().preemptive().basic(user, pass).
                when().get(baseURL).
                then().body("results.state", hasItem("DELIVERY_ATTEMPTED")).statusCode(200);
    }

    @Test
    public void testMessageStateReceived() throws Exception {
        String baseURI = baseURL;
        Object result = null;
        given().log().all().
                queryParam("appId", appId).
                queryParam("size", "50").
                queryParam("searchby", "state").
                queryParam("value", "RECEIVED").
                authentication().preemptive().basic(user, pass).
                when().get(baseURL).
                then().body("results.state", hasItem("RECEIVED")).statusCode(200);
    }

    @Test
    public void testMessageStatePending() throws Exception {
        String baseURI = baseURL;
        Object result = null;
        given().log().all().
                queryParam("appId", appId).
                queryParam("size", "50").
                queryParam("searchby", "state").
                queryParam("value", "PENDING").
                authentication().preemptive().basic(user, pass).
                when().get(baseURL).
                then().body("results.state", hasItem("PENDING")).statusCode(200);
    }

    @Test
    public void testMessageStateWakeupRequired() throws Exception {
        String baseURI = baseURL;
        Object result = null;
        given().log().all().
                queryParam("appId", appId).
                queryParam("size", "50").
                queryParam("searchby", "state").
                queryParam("value", "WAKEUP_REQUIRED").
                authentication().preemptive().basic(user, pass).
                when().get(baseURL).
                then().body("results.state", hasItem("WAKEUP_REQUIRED")).statusCode(200);
    }

    @Test
    public void testMessageStateWakeupSent() throws Exception {
        String baseURI = baseURL;
        Object result = null;
        given().log().all().
                queryParam("appId", appId).
                queryParam("size", "50").
                queryParam("searchby", "state").
                queryParam("value", "WAKEUP_SENT").
                authentication().preemptive().basic(user, pass).
                when().get(baseURL).
                then().body("results.state", hasItem("WAKEUP_SENT")).statusCode(200);
    }
}